/* log.c - Log to logcat.
 *
 * created 2016 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *

    usage: log [-p PRI] [-t TAG] MESSAGE...

    Logs message to logcat.

    -p  use the given priority instead of INFO:
      d: DEBUG  e: ERROR  f: FATAL  i: INFO  v: VERBOSE  w: WARN  s: SILENT
    -t  use the given tag instead of "log"
*/

#include "glue.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <android/log.h>

char *tag;
char *pri;
char buffer[4096];

int main(int argc, char **argv)
{

  int c;
  int errflg = 0;

  while ((c = getopt(argc, argv, ":p:t:")) != -1) {
     switch(c) {
     case 'p':
       pri = optarg;
       break;
     case 't':
       tag = optarg;
       break;
     case ':':       /* -f or -o without operand */
       fprintf(stderr,
           "Option -%c requires an operand\n", optopt);
       errflg++;
       break;
     case '?':
       fprintf(stderr,
           "Unrecognized option: '-%c'\n", optopt);
       errflg++;
     }
  }
  if (errflg) {
     fprintf(stderr, "usage: log [-p PRI] [-t TAG] MESSAGE... ");
     exit(2);
  }

  char *s = buffer;
  int i;
  android_LogPriority priority = ANDROID_LOG_INFO;

  if (pri) {
    i = stridx("defisvw", tolower(*pri));
    if (i==-1 || strlen(pri)!=1) error_exit("bad -p '%s'", pri);
    priority = (android_LogPriority []){ANDROID_LOG_DEBUG, ANDROID_LOG_ERROR,
      ANDROID_LOG_FATAL, ANDROID_LOG_INFO, ANDROID_LOG_SILENT,
      ANDROID_LOG_VERBOSE, ANDROID_LOG_WARN}[i];
  }
  if (!tag) tag = "log";

  for (/*optind*/; optind < argc; optind++) {
    if (i > 1) *s++ = ' ';
    if ((s-buffer)+strlen(argv[optind])>=1024) {
      memcpy(s, argv[optind], 1024-(s-buffer));
      buffer[1024] = 0;
      error_msg("log cut at 1024 bytes");

      break;
    }
    s = stpcpy(s, argv[optind]);
  }

  __android_log_write(priority, tag, buffer);
  
  return 0;
}
