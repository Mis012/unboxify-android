/* setenforce.c - Set the current SELinux mode
 *
 * created 2014 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
*/

#include "glue.h"

#include <stdio.h>

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>

int main(int argc, char **argv)
{
  char *new = argv[1];
  int state, ret;

  if (!is_selinux_enabled()) error_exit("SELinux is disabled");
  else if (!strcmp(new, "1") || !strcasecmp(new, "enforcing")) state = 1;
  else if (!strcmp(new, "0") || !strcasecmp(new, "permissive")) state = 0;
  else error_exit("Invalid state: %s", new);

  ret = security_setenforce(state);
  if (ret == -1) perror_exit("Couldn't set enforcing status to '%s'", new);
  
  return 0;
}
