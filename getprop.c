/* getprop.c - Get an Android system property
 *
 * created 2015 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
*/

#include "glue.h"

#include <stdio.h>
#include <stdlib.h>

#include <sys/system_properties.h>

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>

  size_t size;
  char **nv; // name/value pairs: even=name, odd=value
  struct selabel_handle *handle;
  int flag_Z = 0;

static char *get_property_context(const char *property)
{
  char *context = NULL;

  if (selabel_lookup(handle, &context, property, 1)) {
    perror_exit("unable to lookup label for \"%s\"", property);
  }
  return context;
}

static void add_property(const prop_info *pi, void *unused)
{
  char name[PROP_NAME_MAX];
  char value[PROP_VALUE_MAX];
  __system_property_read(pi, name, value);

  if (!(size&31)) nv = xrealloc(nv, (size+32)*2*sizeof(char *));

  nv[2*size] = xstrdup((char *)name);
  if (flag_Z) {
    nv[1+2*size++] = get_property_context(name);
  } else {
    nv[1+2*size++] = xstrdup((char *)value);
  }
}

// Needed to supress extraneous "Loaded property_contexts from" message
static int selinux_log_callback_local(int type, const char *fmt, ...)
{
  va_list ap;

  if (type == SELINUX_INFO) return 0;
  va_start(ap, fmt);
  verror_msg((char *)fmt, 0, ap);
  va_end(ap);
  return 0;
}

int main(int argc, char **argv)
{
  if (argc > 2) {
    if(!strcmp(argv[2], "-Z")) {
      flag_Z = 1;
    }
  }

  if (flag_Z) {
    union selinux_callback cb;

    cb.func_log = selinux_log_callback_local;
    selinux_set_callback(SELINUX_CB_LOG, cb);
    handle = selinux_android_prop_context_handle();
    if (!handle) error_exit("unable to get selinux property context handle");
  }

  if (argv[1]) {
    if (flag_Z) {
      char *context = get_property_context(argv[1]);

      puts(context);
      free(context);
    } else {
      char value[PROP_VALUE_MAX];
      __system_property_get(argv[1], value);
      puts(value);
    }
  } else {
    size_t i;

    if (__system_property_foreach(add_property, NULL))
      error_exit("property_list");
    qsort(nv, size, 2*sizeof(char *), qstrcmp);
    for (i = 0; i<size; i++) printf("[%s]: [%s]\n", nv[i*2],nv[1+i*2]);
    
    for (i = 0; i<size; i++) {
      free(nv[i*2]);
      free(nv[1+i*2]);
    }
    free(nv);
    
  }
  if (flag_Z) selabel_close(handle);
  
  return 0;
}
