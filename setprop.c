/* setprop.c - Set an Android system property
 *
 * created 2015 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
*/

#include "glue.h"

#include <ctype.h>

#include <sys/system_properties.h>

int main(int argc, char **argv)
{
  if(argc < 2) {
    error_exit("too few arguments");
  }
  char *name = argv[1], *value = argv[2];
  char *p;
  size_t name_len = strlen(name), value_len = strlen(value);

  // property_set doesn't tell us why it failed, and actually can't
  // recognize most failures (because it doesn't wait for init), so
  // we duplicate all of init's checks here to help the user.

  if (value_len >= PROP_VALUE_MAX && !strncmp(value, "ro.", 3))
    error_exit("value '%s' too long; try '%.*s'",
               value, PROP_VALUE_MAX - 1, value);

  if (*name == '.' || name[name_len - 1] == '.')
    error_exit("property names must not start or end with '.'");
  if (strstr(name, ".."))
    error_exit("'..' is not allowed in a property name");
  for (p = name; *p; ++p)
    if (!isalnum(*p) && !strchr(":@_.-", *p))
      error_exit("invalid character '%c' in name '%s'", *p, name);

  if (__system_property_set(name, value))
    error_exit("failed to set property '%s' to '%s'", name, value);
    
  return 0;
}
