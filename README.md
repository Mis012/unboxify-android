# unboxify-android - make toybox obsolete
standalone version of android tools otherwise only available via toybox, for use alongside busybox.
includes: 
* getenforce.c
* getprop_26.c - only works from API 26 up (but it's not deprecated :P )
* getprop.c
* glue.c - includes some non-standard wrappers toybox applets like to use. Could be gotten rid of completely with a bit of work.
* glue.h
* load_policy.c
* log.c
* Makefile
* restorecon.c
* runcon.c
* sendevent.c
* setenforce.c
* setprop.c
* start.c - creates one executable `start_stop`, symlink to `start` and `stop` to use.

# license
all my edits are GPLv3, go to toybox if you want just BSD0. This codebase should be considered GPLv3 infected.
why?
ToyBox as a BSD0 alternative to BusyBox is just a disgusting idea, and as such I don't feel like using their license. I don't like the project, but these tools were only available there - so I made them standalone, so now they're available here as well. Now I can use busybox on my phone without missing out on these tools. I consider this to have an added value, and it's not supposed to get merged back - so I chose a license I like.
