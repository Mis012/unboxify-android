export CC=~/Android/ndk-standalone/bin/arm-linux-androideabi-gcc
export FLAGS_COMMON=-pie -fPIC -Wall

default: all

getprop: getprop.c glue.c glue.h
	$(CC) getprop.c glue.c -lselinux -o getprop $(FLAGS_COMMON)
setprop: setprop.c glue.c glue.h
	$(CC) setprop.c glue.c -o setprop $(FLAGS_COMMON)
getenforce: getenforce.c glue.c glue.h
	$(CC) getenforce.c glue.c -lselinux -o getenforce $(FLAGS_COMMON)
setenforce: setenforce.c glue.c glue.h
	$(CC) setenforce.c glue.c -lselinux -o setenforce $(FLAGS_COMMON)
start: start.c glue.c glue.h
	$(CC) start.c glue.c -o start_stop $(FLAGS_COMMON)
log: log.c glue.c glue.h
	$(CC) log.c glue.c -llog -o log $(FLAGS_COMMON)
sendevent: sendevent.c glue.c glue.h
	$(CC) sendevent.c glue.c -o sendevent $(FLAGS_COMMON)
load_policy: load_policy.c glue.c glue.h
	$(CC) load_policy.c glue.c -lselinux -o load_policy $(FLAGS_COMMON)
runcon: runcon.c glue.c glue.h
	$(CC) runcon.c glue.c -lselinux -o runcon $(FLAGS_COMMON)
restorecon: restorecon.c glue.c glue.h
	$(CC) restorecon.c glue.c -lselinux -o restorecon $(FLAGS_COMMON)

all: getprop setprop getenforce setenforce start log sendevent load_policy runcon restorecon

clean:
	rm getprop setprop getenforce setenforce start_stop log sendevent load_policy runcon restorecon
