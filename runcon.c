/* runcon.c - Run command in specified security context
 *
 * created 2015 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
    usage: runcon CONTEXT COMMAND [ARGS...]

    Run a command in a specified security context.
*/

#include "glue.h"

#include <unistd.h>

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>

int main(int argc, char **argv)
{
  if(argc < 2) {
  	error_exit("too few arguments\nusage: runcon CONTEXT COMMAND [ARGS...]");
  }

  char *context = argv[1];

  if (setexeccon(context)) perror_exit("Could not set context to %s", context);

  execvp(argv[2], (argv + 2));
  
  return 0;
}
