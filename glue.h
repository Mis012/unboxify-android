/* glue.h - glue to replace / serve functions from toybox/lib (header)
 *
 * part created and part copied/edited from toybox/lib & Copyright 2018 Mis012
 * made for standalone version of android specific 'toys'/applets (for use alongside busybox), licensed under GPLv3
 *
*/

#ifndef GLUE_H
#define GLUE_H

#include <stdlib.h>
#include <unistd.h>

#define ARRAY_LEN(array) (sizeof(array)/sizeof(*array))

int qstrcmp(const void *p1, const void *p2);
void verror_msg(char *msg, int err, va_list va);
void error_exit(char *msg, ...) __attribute__ ((format(printf, 1, 2) noreturn));
void perror_exit(char *msg, ...) __attribute__ ((format(printf, 1, 2) noreturn));
void error_msg(char *msg, ...) __attribute__ ((format(printf, 1, 2) noreturn));
void perror_msg(char *msg, ...) __attribute__ ((format(printf, 1, 2) noreturn));
void *xrealloc(void *ptr, size_t size);
char *xstrndup(char *s, size_t n);
char *xstrdup(char *s);
int stridx(char *haystack, char needle);
off_t fdlength(int fd);

#endif
