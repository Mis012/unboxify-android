/* getenforce.c - Get the current SELinux mode
 *
 * created 2014 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
*/

#include "glue.h"

#include <stdio.h>

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>

int main(int argc, char **argv)
{
  if (!is_selinux_enabled()) puts("Disabled");
  else {
    int ret = security_getenforce();

    if (ret == -1) perror_exit("Couldn't get enforcing status");
    else puts(ret ? "Enforcing" : "Permissive");
  }
  return 0;
}
