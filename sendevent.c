/* sendevent.c - Send Linux input events.
 *
 * created 2016 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
 
    usage: sendevent DEVICE TYPE CODE VALUE

    Sends a Linux input event.
*/

#include "glue.h"

#include <stdio.h>
#include <fcntl.h>

#include <linux/input.h>

int main(int argc, char **argv)
{
  int fd = open(argv[1], O_RDWR);
  int version;
  struct input_event ev;

  if (ioctl(fd, EVIOCGVERSION, &version))
    perror_exit("EVIOCGVERSION failed for %s", argv[1]);
  
  memset(&ev, 0, sizeof(ev));
  // TODO: error checking and support for named constants.
  ev.type = atoi(argv[2]);
  ev.code = atoi(argv[3]);
  ev.value = atoi(argv[4]);
  write(fd, &ev, sizeof(ev));
  
  return 0;
}
