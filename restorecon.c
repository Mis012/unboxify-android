/* restorecon.c - Restore default security contexts for files
 *
 * created 2015 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
    usage: restorecon [-D] [-F] [-R] [-n] [-v] FILE...

    Restores the default security contexts for the given files.

    -D	apply to /data/data too
    -F	force reset
    -R	recurse into directories
    -n	don't make any changes; useful with -v to see what would change
    -v	verbose: show any changes
*/

#include "glue.h"

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>


int main(int argc, char **argv)
{
  int c;
  int errflg = 0;
  
  char **s;
  int flags = 0;
  while ((c = getopt(argc, argv, ":DFRrnv")) != -1) {
     switch(c) {
     case 'D':
       flags |= SELINUX_ANDROID_RESTORECON_DATADATA;
       break;
     case 'F':
       flags |= SELINUX_ANDROID_RESTORECON_FORCE;
       break;
     case 'R': //fall trough
     case 'r':
       flags |= SELINUX_ANDROID_RESTORECON_RECURSE;
       break;
     case 'n':
       flags |= SELINUX_ANDROID_RESTORECON_NOCHANGE;
       break;
     case 'v':
       flags |= SELINUX_ANDROID_RESTORECON_VERBOSE;
       break;
     case '?':
       fprintf(stderr,
           "Unrecognized option: '-%c'\n", optopt);
       errflg++;
     }
  }
  if (errflg) {
     fprintf(stderr, "usage: restorecon [-D] [-F] [-R/-r] [-n] [-v] FILE...");
     exit(2);
  }

  for (s = (argv + optind); *s; s++)
    if (selinux_android_restorecon(*s, flags) < 0)
      perror_msg("restorecon failed: %s", *s);
      
  return 0;
}
