/* start.c - Start/stop system services.
 *
 * created 2016 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
*/

#include "glue.h"

#include <unistd.h>
#include <sys/types.h>

#include <sys/system_properties.h>

static void start_stop(int start, char **argv)
{
  char *property = start ? "ctl.start" : "ctl.stop";
  // null terminated in both directions
  char *services[] = {0,"netd","surfaceflinger","zygote","zygote_secondary",0};
  char **ss = (argv + 1); //as in &(argv[1])
  int direction = 1;

  if (getuid()) error_exit("must be root");

  if (!*ss) {
    // If we don't have optargs, iterate through services forward/backward.
    ss = services+1;
    if (!start) ss = services+ARRAY_LEN(services)-2, direction = -1;
  }

  for (; *ss; ss += direction)
    if (__system_property_set(property, *ss))
      error_exit("failed to set property '%s' to '%s'", property, *ss);
}

int main(int argc, char **argv)
{
  if(!strcmp(argv[0], "start")) {
    start_stop(1, argv);
  } else if(!strcmp(argv[0], "stop")) {
    start_stop(0, argv);
  } else {
    error_exit("unsupported argv[0]: %s, symlink this binary to 'start' and 'stop' to perform those actions, respectively", argv[0]);
  }

  return 0;
}
