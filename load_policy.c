/* load_policy.c - Load a policy file
 *
 * created 2015 The Android Open Source Project under BSD0
 * edited & Copyright 2018 Mis012
 * this is a standalone version (for use alongside busybox), licensed under GPLv3
 *
 
    usage: load_policy FILE

    Load the specified policy file.
*/

#include "glue.h"

#include <fcntl.h>
#include <sys/mman.h>

#include <selinux/android.h>
#include <selinux/label.h>
#include <selinux/selinux.h>

int main(int argc, char **argv)
{
  int fd = open(argv[1], O_RDONLY);
  if (fd == -1) perror_exit("open() failed");
  off_t policy_len = fdlength(fd);
  char *policy_data = mmap(0, policy_len, PROT_READ, MAP_PRIVATE, fd, 0);
  if (policy_data == MAP_FAILED) perror_exit("mmap() failed");

  close(fd);
  if (security_load_policy(policy_data, policy_len) < 0)
    perror_exit("security_load_policy %s", argv[1]);

  munmap(policy_data, policy_len);

  return 0;
}
